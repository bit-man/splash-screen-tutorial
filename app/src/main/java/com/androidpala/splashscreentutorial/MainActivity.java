package com.androidpala.splashscreentutorial;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends Activity {


    private int flag = 1;
    private String TAG = "MainActivty";
    private Dialog LoadingDialog;

    //Splash screen with network operation
    TextView LoadingMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         //lets try first method
        //Method 1:  Activty Method Without Network Operations


        Thread counterThread = new Thread() {
           public void run() {

               try {

                 while(flag <= 4) {
                    Log.d("Splash", "flag " + flag);
                    sleep(1000);
                    flag++;
                 }

              } catch (Exception e) {

              }
              finally {

                Log.d("Timer", "flag " + flag);
                Intent ii = new Intent(MainActivity.this,
                        ProfileActivity.class);
                startActivity(ii);
                finish();
              }
           }
        };
        counterThread.start();



        //Method 2: Splash screen with network operation

        ////new MakeNetworkCall().execute();

    }


    private class MakeNetworkCall extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg) {

            InputStream is = null;

            String res = "";

            String url = "http://androidpala.com/tutorial/splash.php";

            res = FetchDataFromServer(url);

            return res;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equals("OK")) {

                Toast.makeText(getApplicationContext(), "OK", Toast.LENGTH_LONG).show();

                Intent ii = new Intent(MainActivity.this,
                        ProfileActivity.class);
                startActivity(ii);

                finish();

            } else {

                LoadingMsg = (TextView) findViewById(R.id.textView);
                LoadingMsg.setText("Server Error");

            }

        }
    }

    public String FetchDataFromServer(String data_url) {

        String Response = "";

        InputStream DataInputStream = null;
        try {

            URL url = new URL(data_url);
            HttpURLConnection cc = (HttpURLConnection)
                    url.openConnection();
            //set timeout for reading InputStream
            cc.setReadTimeout(5000);
            // set timeout for connection
            cc.setConnectTimeout(5000);
            //set HTTP method to GET
            cc.setRequestMethod("GET");
            //set it to true as we are connecting for input
            cc.setDoInput(true);

            //reading HTTP response code
            int response = cc.getResponseCode();

            //if response code is 200 / OK then read Inputstream
            if (response == HttpURLConnection.HTTP_OK) {
                DataInputStream = cc.getInputStream();
                Response = StreamToString(DataInputStream);
            }

        } catch (Exception e) {
            Log.e(TAG, "Error in GetData", e);
        }


        return Response;
    }

    String StreamToString(InputStream stream) {

        InputStreamReader isr = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder response = new StringBuilder();

        String line = null;
        try {

            while ((line = reader.readLine()) != null) {
                response.append(line);
            }

        } catch (IOException e) {
            Log.e(TAG, "Error in StreamToString", e);
        } catch (Exception e) {
            Log.e(TAG, "Error in StreamToString", e);
        } finally {

            try {
                stream.close();

            } catch (IOException e) {
                Log.e(TAG, "Error in StreamToString", e);

            } catch (Exception e) {
                Log.e(TAG, "Error in StreamToString", e);
            }
        }
        return response.toString();


    }
}
